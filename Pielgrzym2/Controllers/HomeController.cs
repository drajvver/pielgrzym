﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Domain.Abstract;
using Domain.Concrete;
using Domain.Entities;
using Newtonsoft.Json;
using Pielgrzym2.Models;

namespace Pielgrzym2.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _uow;
        public HomeController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Tos()
        {
            return View();
        }

        public ActionResult GetFlats()
        {
            List<FlatsCoordinatesViewModel> flatsCoordinatesViewModels = new List<FlatsCoordinatesViewModel>();
            var advertisements = _uow.AdvertisementRepository.GetAllAdvertisement();
            foreach (var advertisement in advertisements)
            {
                if (advertisement.IsActive && advertisement.ExpirationTime > DateTime.UtcNow)
                {
                    FlatsCoordinatesViewModel flatsCoordinatesViewModel = new FlatsCoordinatesViewModel();
                    flatsCoordinatesViewModel.AdvertisementId = advertisement.Id;
                    flatsCoordinatesViewModel.Lat = advertisement.Latitude;
                    flatsCoordinatesViewModel.Lng = advertisement.Longitude;

                    if (advertisement.BedsTaken > 1)
                    {
                        flatsCoordinatesViewModel.AdStatus = 1; // iles zajete, iles wolne
                    }
                    if (advertisement.BedsTaken == advertisement.BedsAmount)
                    {
                        flatsCoordinatesViewModel.AdStatus = -1; // 0 dostepnych
                    }
                    if (advertisement.BedsTaken == 0)
                    {
                        flatsCoordinatesViewModel.AdStatus = 0; // wszystkie lozka dostepne
                    }

                    flatsCoordinatesViewModels.Add(flatsCoordinatesViewModel);
                }
            }
            return Json(flatsCoordinatesViewModels, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Contact()
        {
            ContactViewModel model = new ContactViewModel();
            model.OwnerMail = "kontakt@przyjmijpielgrzyma.pl";

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(ContactViewModel model)
        {
            model.Message = model.Message.Replace("\n", "<br/>");

            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(model.OwnerMail);
                mail.ReplyToList.Add(model.UserMail);
                mail.From = new MailAddress("mailer@przyjmijpielgrzyma.pl", "Formularz Kontaktowy");
                mail.Body = model.Message;
                mail.Subject = "Wiadomość kontaktowa z PrzyjmijPielgrzyma.pl";
                mail.IsBodyHtml = true;

                SmtpClient sender = new SmtpClient("smtp.zoho.com");
                var c = new NetworkCredential("mailer@przyjmijpielgrzyma.pl", "Responder40%");
                sender.EnableSsl = true;
                sender.Port = 587;
                sender.UseDefaultCredentials = false;
                sender.DeliveryMethod = SmtpDeliveryMethod.Network;
                sender.Credentials = c;
                await sender.SendMailAsync(mail);
            }
            catch (Exception ex)
            {
                TempData["poprawnie"] = false;
                TempData["wiadomosc"] = "Nie udało się wysłać wiadomości, spróbuj ponownie później.";
            }

            TempData["poprawnie"] = true;
            TempData["wiadomosc"] = "Poprawnie wysłano wiadomość.";

            return RedirectToAction("Index", "Home");
        }
    }
}