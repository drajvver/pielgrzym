﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Entities;
using Domain.Services;
using Microsoft.AspNet.Identity;
using Pielgrzym2.Models;

namespace Pielgrzym2.Controllers
{
    public class AdvertisementController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly CoordinateService _coordinateService;
        private readonly UploadFileService _uploadFileService;

        public AdvertisementController(IUnitOfWork uow, CoordinateService coordinateService, UploadFileService uploadFileSerivce)
        {
            _uow = uow;
            _coordinateService = coordinateService;
            _uploadFileService = uploadFileSerivce;
        }

        // GET: Advertisement
        public ActionResult Index()
        {
            var ads = _uow.AdvertisementRepository.GetAllAdvertisementByUserId(User.Identity.GetUserId());

            var model = new List<AdvertisementListViewModel>();

            foreach (var advertisement in ads)
            {
                AdvertisementListViewModel vm = new AdvertisementListViewModel
                {
                    Id = advertisement.Id,
                    Street = advertisement.Street,
                    AdContent = advertisement.AdContent,
                    BedsAmount = advertisement.BedsAmount,
                    BedsTaken = advertisement.BedsTaken,
                    City = advertisement.City,
                    ExpirationDate = advertisement.ExpirationTime,
                    IsActive = advertisement.IsActive,
                    PostalCode = advertisement.PostalCode,
                    PricePerBed = advertisement.PricePerBed
                };

                model.Add(vm);

            }

            return View(model);
        }

        [Authorize]
        public ActionResult Create()
        {
            AddAdverisementViewModel model = new AddAdverisementViewModel();

            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AddAdverisementViewModel model)
        {
            if (ModelState.IsValid)
            {
                Advertisement advertisement = new Advertisement
                {
                    AdContent = model.AdContent,
                    BedsAmount = model.BedsAmount,
                    City = model.City,
                    IsActive = false,
                    PostalCode = model.PostalCode,
                    PricePerBed = model.PricePerBed,
                    Street = model.Street,
                    User = _uow.ApplicationUserRepository.FindUserById(User.Identity.GetUserId()),
                    CreationTime = DateTime.UtcNow, // TODO
                    ExpirationTime = DateTime.UtcNow // TODO
                };
                try
                {
                    // set coordinates
                    var coordinates =
                        _coordinateService.GetCoordinates(advertisement.Street + ", " + advertisement.PostalCode + ", " + advertisement.City);
                    advertisement.Latitude = coordinates.Lat;
                    advertisement.Longitude = coordinates.Lng;
                }
                catch (Exception ex)
                {
                    TempData["blad"] = "Wystąpił błąd - sprawdź wpisane dane i spróbuj ponownie.";
                    TempData["model"] = model;

                    return View(model);
                }

                _uow.AdvertisementRepository.AddNewAdvertisement(advertisement);

                UsedSmsCode code = new UsedSmsCode
                {
                    Advertisement = advertisement,
                    SmsCode = model.SmsServiceCode.SmsCode,
                    Value = model.SmsServiceCode.Value
                };

                if (!_uow.UsedSmsCodeRepository.AddUsedCode(code))
                {
                    TempData["blad"] = "Wystąpił błąd - wpisany kod SMS jest błędny lub został już wykorzystany!";
                    TempData["model"] = model;

                    return View(model);
                }

                advertisement.IsActive = true;

                switch (code.Value)
                {
                    case -5:
                        advertisement.ExpirationTime = advertisement.CreationTime.AddMonths(1);
                        break;
                    case -10:
                        advertisement.ExpirationTime = advertisement.CreationTime.AddMonths(3);
                        break;
                    case -15:
                        advertisement.ExpirationTime = advertisement.CreationTime.AddMonths(6);
                        break;
                    case -20:
                        advertisement.ExpirationTime = advertisement.CreationTime.AddMonths(12);
                        break;
                    case 10:
                        advertisement.ExpirationTime = advertisement.CreationTime.AddMonths(1);
                        break;
                    case 15:
                        advertisement.ExpirationTime = advertisement.CreationTime.AddMonths(3);
                        break;
                    case 20:
                        advertisement.ExpirationTime = advertisement.CreationTime.AddMonths(6);
                        break;
                    case 30:
                        advertisement.ExpirationTime = advertisement.CreationTime.AddMonths(12);
                        break;
                    default:
                        advertisement.ExpirationTime = DateTime.UtcNow;
                        break;
                }

                _uow.AdvertisementRepository.EditBeds(advertisement);

                return RedirectToAction("FilesUploader", "Advertisement", new { id = advertisement.Id });
            }
            return View(model);
        }

        [Authorize]
        public ActionResult FilesUploader(int id)
        {
            var advertisement = _uow.AdvertisementRepository.FindById(id);
            if (advertisement != null && advertisement.User.Id == User.Identity.GetUserId())
            {
                return View(advertisement);                
            }
            return HttpNotFound();
        }

        [Authorize]
        public ActionResult Delete(int? id)
        {
            if(id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var ad = _uow.AdvertisementRepository.FindById((int) id);
            if (ad == null || ad.User.Id != User.Identity.GetUserId())
            {
                return HttpNotFound();
            }

            return View(ad);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Delete(int id)
        {
            var ad = _uow.AdvertisementRepository.FindById(id);
            _uow.AdvertisementRepository.RemoveAdvertisement(ad);
            TempData["message"] = "Usunięto ogłoszenie.";
            return RedirectToAction("Index");
        }

        [Authorize]
        [HttpPost]
        public ActionResult UploadFiles(int id)
        {
            var advertisement = _uow.AdvertisementRepository.FindById(id);
            if (advertisement == null || advertisement.User.Id != User.Identity.GetUserId())
            {
                return HttpNotFound();
            }

            var isSavedSuccessfully = false;
            var savedFileName = "";

            foreach (string fileName in Request.Files)
            {
                var file = Request.Files[fileName];

                //If the file is "correct" then upload it
                if (file != null && file.ContentLength > 0)
                {
                    //Check for valid file type!
                    if (_uploadFileService.IsFileAllowedForUpload(file, UploadFileService.AllowedImageMimeTypes))
                    {

                        var fileEntity = _uploadFileService.UploadFile(file,
                                                    Server.MapPath(@"~/Files"),advertisement);
                        _uow.FileRepository.Add(fileEntity);

                        isSavedSuccessfully = true;
                        savedFileName = fileEntity.FileName;
                    }
                }

                //Since we allow only one file per request
                break;
            }

            _uow.Commit();

            return Json(isSavedSuccessfully ? new { Message = savedFileName } : new { Message = "Error in saving file" });
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var ad = _uow.AdvertisementRepository.FindById((int)id);
            if (ad == null)
            {
                return HttpNotFound();
            }
            DetailAdvertisementViewModel detailAdvertisementViewModel = new DetailAdvertisementViewModel
            {
                AdContent = ad.AdContent,
                BedsAmount = ad.BedsAmount,
                BedsTaken = ad.BedsTaken,
                City = ad.City,
                CreationTime = ad.CreationTime,
                Id = ad.Id,
                Images = ad.ImagesCollection.ToList(),
                PostalCode = ad.PostalCode,
                PricePerBed = ad.PricePerBed,
                Street = ad.Street,
                Latitude = ad.Latitude,
                Longitude = ad.Longitude
            };



            return PartialView(detailAdvertisementViewModel);
        }

        [Authorize]
        public ActionResult RefreshFile(int advertisementId)
        {
            var advertisement = _uow.AdvertisementRepository.FindById(advertisementId);
            if (advertisement == null)
                return HttpNotFound();
            var files = _uow.FileRepository.GetFilesByAdvertisement(advertisement);
            return PartialView("_uploadedFiles", files);
        }

        [HttpGet]
        public ActionResult Contact(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var ad = _uow.AdvertisementRepository.FindById((int)id);
            if (ad == null)
            {
                return HttpNotFound();
            }

            ContactViewModel model = new ContactViewModel();
            model.OwnerMail = ad.User.Email;

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(ContactViewModel model)
        {
            model.Message = model.Message.Replace("\n", "<br/>");

            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(model.OwnerMail);
                mail.ReplyToList.Add(model.UserMail);
                mail.From = new MailAddress("mailer@przyjmijpielgrzyma.pl", "Portal PrzyjmijPielgrzyma.pl");
                mail.Body = model.Message;
                mail.Subject = "Wiadomość kontaktowa z PrzyjmijPielgrzyma.pl";
                mail.IsBodyHtml = true;

                SmtpClient sender = new SmtpClient("smtp.zoho.com");
                var c = new NetworkCredential("mailer@przyjmijpielgrzyma.pl", "Responder40%");
                sender.EnableSsl = true;
                sender.Port = 587;
                sender.UseDefaultCredentials = false;
                sender.DeliveryMethod = SmtpDeliveryMethod.Network;
                sender.Credentials = c;
                await sender.SendMailAsync(mail);
            }
            catch (Exception ex)
            {
                TempData["poprawnie"] = false;
                TempData["wiadomosc"] = "Nie udało się wysłać wiadomości, spróbuj ponownie.";
            }

            TempData["poprawnie"] = true;
            TempData["wiadomosc"] = "Poprawnie wysłano wiadomość.";
            
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Advertisement model = null;
            if (id != null)
            {
                model = _uow.AdvertisementRepository.FindById((int) id);
                if (model == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            else
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Advertisement model)
        {
            var ad = _uow.AdvertisementRepository.FindById(model.Id);
            if (model.BedsTaken != ad.BedsTaken)
            {
                if (model.BedsTaken > ad.BedsAmount)
                {
                    TempData["editedSuccess"] =
                        "Ilość zajętych łóżek nie może być większa niż ilość dostępnych łóżek!";
                    return RedirectToAction("Edit", new {id = model.Id});
                }

                ad.BedsTaken = model.BedsTaken;

                _uow.AdvertisementRepository.EditBeds(ad);

                TempData["editedSuccess"] = "Ilość łóżek zmieniona pomyślnie.";
            }
            else
            {
                TempData["editedSuccess"] = "Ilość łóżek pozostała bez zmian.";
            }

            return RedirectToAction("Index");
            
        }

        [Authorize]
        [HttpGet]
        public ActionResult Prolong(int? id)
        {
            ProlongAdvertisementViewModel model = new ProlongAdvertisementViewModel();

            if (TempData["model"] != null)
            {
                model = TempData["model"] as ProlongAdvertisementViewModel;
            }

            if (id != null)
            {
                Advertisement ad = _uow.AdvertisementRepository.FindById((int) id);
                model.Id = (int) id;
                model.OldExpirationDate = ad.ExpirationTime;
                model.BedsAmount = ad.BedsAmount;
            }

            return View(model);
        }

        [Authorize]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Prolong(ProlongAdvertisementViewModel model)
        {
            var advertisement = _uow.AdvertisementRepository.FindById(model.Id);

            UsedSmsCode code = new UsedSmsCode
            {
                Advertisement = advertisement,
                SmsCode = model.SmsServiceCode.SmsCode,
                Value = model.SmsServiceCode.Value
            };

            if (!_uow.UsedSmsCodeRepository.AddUsedCode(code))
            {
                model.OldExpirationDate = advertisement.ExpirationTime;
                TempData["blad"] = "Wystąpił błąd - wpisany kod SMS jest błędny lub został już wykorzystany!";
                TempData["model"] = model;

                return View(model);
            }

            switch (code.Value)
            {
                case -5:
                    advertisement.ExpirationTime = advertisement.ExpirationTime.AddMonths(1);
                    break;
                case -10:
                    advertisement.ExpirationTime = advertisement.ExpirationTime.AddMonths(3);
                    break;
                case -15:
                    advertisement.ExpirationTime = advertisement.ExpirationTime.AddMonths(6);
                    break;
                case -20:
                    advertisement.ExpirationTime = advertisement.ExpirationTime.AddMonths(12);
                    break;
                case 10:
                    advertisement.ExpirationTime = advertisement.ExpirationTime.AddMonths(1);
                    break;
                case 15:
                    advertisement.ExpirationTime = advertisement.ExpirationTime.AddMonths(3);
                    break;
                case 20:
                    advertisement.ExpirationTime = advertisement.ExpirationTime.AddMonths(6);
                    break;
                case 30:
                    advertisement.ExpirationTime = advertisement.ExpirationTime.AddMonths(12);
                    break;
                default:
                    advertisement.ExpirationTime = DateTime.UtcNow;
                    break;
            }

            advertisement.IsActive = true;

            _uow.AdvertisementRepository.EditBeds(advertisement);

            TempData["editedSuccess"] = "Pomyślnie przedłużono ważność ogłoszenia. Nowy termin ważności to: <b>" +
                                        advertisement.ExpirationTime.ToString("D") + "</b>";

            return RedirectToAction("Index");
        }
    }
}