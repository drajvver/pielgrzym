﻿using System.Web.Mvc;

namespace Pielgrzym2.Extensions
{
    public class CustomViewEngine : RazorViewEngine
    {
        public CustomViewEngine()
        {
            var viewLocations = new[] {  
            "~/Views/{1}/{0}.cshtml",
            "~/Views/{1}/Partial/{0}.cshtml",
            "~/Views/Shared/{0}.cshtml"
            // etc
        };

            this.PartialViewLocationFormats = viewLocations;
            this.ViewLocationFormats = viewLocations;
        }
    }
}