﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Domain.Entities;

namespace Pielgrzym2.Models
{
    public class AddAdverisementViewModel
    {
        [Required]
        [Display(Name = "Treść ogłoszenia")]
        public string AdContent { get; set; }

        [Required]
        [Display(Name = "Ilośc miejsc")]
        public int BedsAmount { get; set; }

        [Required]
        [Display(Name = "Cena za miejsce")]
        public double PricePerBed { get; set; }

        [Required]
        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Required]
        [Display(Name = "Miasto")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "Kod SMS")]
        public UsedSmsCode SmsServiceCode { get; set; }

        [Display(Name = "Wybierz stawkę SMS")]
        public SelectList SmsValueList 
        {
            get
            {
                return new SelectList(_valuesList.Take(4), "Value", "Text");
            } 
        }

        [Display(Name = "Wybierz stawkę SMS (powyżej 5 miejsc)")]
        public SelectList SmsValueListOverFiveBeds
        {
            get
            {
                return new SelectList(_valuesList.Skip(4), "Value", "Text");
            }
        }

        private List<SmsCodeValuesViewModel> _valuesList
        {
            get
            {
                var list = new List<SmsCodeValuesViewModel>();
                list.Add(new SmsCodeValuesViewModel(-5, "5zł + VAT = miesiąc"));
                list.Add(new SmsCodeValuesViewModel(-10, "10zł + VAT = 3 miesiące"));
                list.Add(new SmsCodeValuesViewModel(-15, "15zł + VAT = 6 miesięcy"));
                list.Add(new SmsCodeValuesViewModel(-20, "20zł + VAT = 12 miesięcy"));

                list.Add(new SmsCodeValuesViewModel(10, "10zł + VAT = miesiąc"));
                list.Add(new SmsCodeValuesViewModel(15, "15zł + VAT = 3 miesiące"));
                list.Add(new SmsCodeValuesViewModel(20, "20zł + VAT = 6 miesięcy"));
                list.Add(new SmsCodeValuesViewModel(30, "30zł + VAT = 12 miesięcy"));

                return list;
            }
        }
    }

    public class SmsCodeValuesViewModel
    {
        public SmsCodeValuesViewModel(int value, string text)
        {
            Value = value;
            Text = text;
        }
        public int Value { get; set; }
        public string Text { get; set; }
    }
}