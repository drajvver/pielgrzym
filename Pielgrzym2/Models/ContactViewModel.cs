﻿using System.ComponentModel.DataAnnotations;

namespace Pielgrzym2.Models
{
    public class ContactViewModel
    {
        [Display(Name = "Twój e-mail")]
        public string UserMail { get; set; }

        public string OwnerMail { get; set; }

        [Display(Name = "Treść wiadomości")]
        public string Message { get; set; }
    }
}