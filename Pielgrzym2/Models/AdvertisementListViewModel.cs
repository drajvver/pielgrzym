﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Pielgrzym2.Models
{
    public class AdvertisementListViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Wygasa")]
        public string ExpirationDateString
        {
            get { return ExpirationDate.ToString("f"); }
        }

        [Display(Name = "Opis")]
        public string ShortContent
        {
            get { return AdContent.Substring(0, AdContent.Length/4) + " [...]"; }
        }

        [Display(Name = "Ilość miejsc")]
        public int BedsAmount { get; set; }

        [Display(Name = "Cena za miejsce")]
        public double PricePerBed { get; set; }

        [Display(Name = "Aktywne?")]
        public bool IsActive { get; set; }

        [Display(Name = "Miejsc zajętych")]
        public int BedsTaken { get; set; }

        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Display(Name = "Miejscowość")]
        public string City { get; set; }

        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }


        public string AdContent { get; set; }

        public DateTime ExpirationDate { get; set; }

        
    }
}