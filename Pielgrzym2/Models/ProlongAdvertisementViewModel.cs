﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Domain.Entities;

namespace Pielgrzym2.Models
{
    public class ProlongAdvertisementViewModel
    {
        public int Id { get; set; } 

        [Display(Name = "Poprzednia data ważności")]
        public DateTime OldExpirationDate { get; set; }
        [Display(Name = "Nowa data ważności")]
        public DateTime NewExpirationDate { get; set; }

        public int BedsAmount { get; set; }

        [Required]
        [Display(Name = "Kod SMS")]
        public UsedSmsCode SmsServiceCode { get; set; }

        [Display(Name = "Wybierz stawkę SMS")]
        public SelectList SmsValueList
        {
            get
            {
                return new SelectList(_valuesList.Take(4), "Value", "Text");
            }
        }

        [Display(Name = "Wybierz stawkę SMS (powyżej 5 miejsc)")]
        public SelectList SmsValueListOverFiveBeds
        {
            get
            {
                return new SelectList(_valuesList.Skip(4), "Value", "Text");
            }
        }

        private List<SmsCodeValuesViewModel> _valuesList
        {
            get
            {
                var list = new List<SmsCodeValuesViewModel>();
                list.Add(new SmsCodeValuesViewModel(-5, "5zł + VAT = miesiąc"));
                list.Add(new SmsCodeValuesViewModel(-10, "10zł + VAT = 3 miesiące"));
                list.Add(new SmsCodeValuesViewModel(-15, "15zł + VAT = 6 miesięcy"));
                list.Add(new SmsCodeValuesViewModel(-20, "20zł + VAT = 12 miesięcy"));

                list.Add(new SmsCodeValuesViewModel(10, "10zł + VAT = miesiąc"));
                list.Add(new SmsCodeValuesViewModel(15, "15zł + VAT = 3 miesiące"));
                list.Add(new SmsCodeValuesViewModel(20, "20zł + VAT = 6 miesięcy"));
                list.Add(new SmsCodeValuesViewModel(30, "30zł + VAT = 12 miesięcy"));

                return list;
            }
        }
    }
}