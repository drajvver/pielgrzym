﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pielgrzym2.Models
{
    public class FlatsCoordinatesViewModel
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int AdvertisementId { get; set; }
        public int AdStatus { get; set; }
    }
}