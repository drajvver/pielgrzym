﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Domain.Entities;

namespace Pielgrzym2.Models
{
    public class DetailAdvertisementViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Data utworzenia")]
        public DateTime CreationTime { get; set; }
        [Display(Name = "Treść ogłoszenia")]
        public string AdContent { get; set; }
        [Display(Name = "Ilość miejsc")]
        public int BedsAmount { get; set; }
        [Display(Name = "Cena za miejsce (PLN)")]
        public double PricePerBed { get; set; }
        [Display(Name = "Miejsc wolnych")]
        public int BedsTaken { get; set; }
        [Display(Name = "Ulica")]
        public string Street { get; set; }
        [Display(Name = "Miasto")]
        public string City { get; set; }
        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public List<File> Images { get; set; }
    }
}