﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pielgrzym2.Startup))]
namespace Pielgrzym2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
