﻿namespace Domain.Abstract
{
    public interface IUnitOfWork
    {
        void Commit();
        IAdvertisementRepository AdvertisementRepository { get; }
        IFileRepository FileRepository { get; }
        IApplicationUserRepository ApplicationUserRepository { get; }
        IUsedSmsCodeRepository UsedSmsCodeRepository { get; }
    }
}
