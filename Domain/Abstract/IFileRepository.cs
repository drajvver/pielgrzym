﻿using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Abstract
{
    public interface IFileRepository
    {
        void Add(File file);
        IEnumerable<File> GetFilesByAdvertisement(Advertisement advertisement);
    }
}