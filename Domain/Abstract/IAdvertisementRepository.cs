﻿using System.Collections.Generic;
using System.Linq;
using Domain.Entities;

namespace Domain.Abstract
{
    public interface IAdvertisementRepository
    {
        IEnumerable<Advertisement> GetAllAdvertisement();

        IEnumerable<Advertisement> GetAllAdvertisementByUserId(string id); 

        void AddNewAdvertisement(Advertisement ad);
        Advertisement FindById(int id);

        void RemoveAdvertisement(Advertisement ad);

        void EditAdvertisement(Advertisement ad);

        void EditBeds(Advertisement ad);
    }
}