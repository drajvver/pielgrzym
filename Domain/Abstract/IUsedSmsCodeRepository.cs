﻿using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Abstract
{
    public interface IUsedSmsCodeRepository
    {
        UsedSmsCode FindByCode(string code);

        bool AddUsedCode(UsedSmsCode code);

        IEnumerable<UsedSmsCode> FindCodesForAdvertisement(int advertisementId);
    }
}