// <auto-generated />
namespace Domain.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class LatLonCoordinates : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(LatLonCoordinates));
        
        string IMigrationMetadata.Id
        {
            get { return "201506111004448_LatLonCoordinates"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
