namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsedSmsCodes_added : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UsedSmsCodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SmsCode = c.String(),
                        Value = c.Int(nullable: false),
                        AdvertisementId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advertisements", t => t.AdvertisementId, cascadeDelete: true)
                .Index(t => t.AdvertisementId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsedSmsCodes", "AdvertisementId", "dbo.Advertisements");
            DropIndex("dbo.UsedSmsCodes", new[] { "AdvertisementId" });
            DropTable("dbo.UsedSmsCodes");
        }
    }
}
