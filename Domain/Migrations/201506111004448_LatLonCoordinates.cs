namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LatLonCoordinates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advertisements", "Latitude", c => c.Double(nullable: false));
            AddColumn("dbo.Advertisements", "Longitude", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Advertisements", "Longitude");
            DropColumn("dbo.Advertisements", "Latitude");
        }
    }
}
