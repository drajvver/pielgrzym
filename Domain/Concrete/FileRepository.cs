﻿using System.Collections.Generic;
using System.Linq;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete
{
    public class FileRepository : IFileRepository
    {
        private readonly ApplicationDbContext _context;

        public FileRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(File file)
        {
            _context.Files.Add(file);
        }

        public IEnumerable<File> GetFilesByAdvertisement(Advertisement advertisement)
        {
            return _context.Files.Where(p => p.Advertisement.Id == advertisement.Id);
        }
    }
}