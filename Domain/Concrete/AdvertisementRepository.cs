﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete
{
    public class AdvertisementRepository : IAdvertisementRepository
    {
        private readonly ApplicationDbContext _context;

        public AdvertisementRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Advertisement> GetAllAdvertisement()
        {
            return _context.Advertisements.ToList();
        }

        public IEnumerable<Advertisement> GetAllAdvertisementByUserId(string id)
        {
            return _context.Advertisements.Where(x => x.User.Id == id).ToList();
        }

        public void AddNewAdvertisement(Advertisement ad)
        {
            if (ad != null)
            {
                if (
                    !_context.Advertisements.Any(
                        c => c.City == ad.City && c.Street == ad.Street && c.PostalCode == ad.PostalCode))
                {
                    _context.Advertisements.Add(ad);
                    _context.SaveChanges();
                }
            }
        }

        public Advertisement FindById(int id)
        {
            return _context.Advertisements.SingleOrDefault(a => a.Id == id);
        }

        public void RemoveAdvertisement(Advertisement ad)
        {
            _context.Advertisements.Remove(ad);
            _context.SaveChanges();
        }

        public void EditAdvertisement(Advertisement ad)
        {
            Advertisement originalAdvertisement = _context.Advertisements.Find(ad.Id);
            if (originalAdvertisement != ad)
            {
                _context.Entry(ad).State = EntityState.Modified;
                _context.SaveChanges();
            }
        }

        public void EditBeds(Advertisement ad)
        {
                _context.Entry(ad).State = EntityState.Modified;
                _context.SaveChanges();
        }
    }
}