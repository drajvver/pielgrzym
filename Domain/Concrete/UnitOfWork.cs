﻿using System;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using Domain.Abstract;

namespace Domain.Concrete
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Commit()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationError in dbEx.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors))
                {
                    Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                }
            }
        }

        public IAdvertisementRepository AdvertisementRepository
        {
            get { return new AdvertisementRepository(_context); }
        }

        public IFileRepository FileRepository
        {
            get { return new FileRepository(_context); }
        }

        public IApplicationUserRepository ApplicationUserRepository
        {
            get { return new ApplicationUserRepository(_context); }
        }

        public IUsedSmsCodeRepository UsedSmsCodeRepository
        {
            get { return new UsedSmsCodeRepository(_context); }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

       
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                _disposed = true;
            }
        }
    }
}
