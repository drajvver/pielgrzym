﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete
{
    public class UsedSmsCodeRepository : IUsedSmsCodeRepository
    {
        private readonly ApplicationDbContext _context;

        public UsedSmsCodeRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public UsedSmsCode FindByCode(string code)
        {
            return _context.UsedSmsCodes.FirstOrDefault(c => c.SmsCode == code);
        }

        public IEnumerable<UsedSmsCode> FindCodesForAdvertisement(int advertisementId)
        {
            return _context.UsedSmsCodes.Where(c => c.AdvertisementId == advertisementId);
        }

        public bool AddUsedCode(UsedSmsCode code)
        {
            if (string.IsNullOrEmpty(code.SmsCode))
            {
                return false;
            }

            if (code != null)
            {
                UsedSmsCode _code = FindByCode(code.SmsCode);

                if ((_code != null) && _code.SmsCode.Equals(code.SmsCode, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }

                if (_code == null && (code.SmsCode != null || code.SmsCode != ""))
                {
                    _context.UsedSmsCodes.Add(code);
                    _context.SaveChanges();
                    return true;
                }

                return false;
            }

            return false;
        }
    }
}