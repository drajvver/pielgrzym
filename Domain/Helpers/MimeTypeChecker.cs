﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Domain.Helpers
{
    public static class MimeTypeChecker
    {

        private static readonly byte[] Jpg = { 0xFF, 0xD8, 0xFF };
        private static readonly byte[] Png = { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 0x00, 0x0D, 0x49, 0x48, 0x44, 0x52 };
        private static readonly byte[] Png2 = { 0xA, 0x1A, 0xA, 0xD, 0x47, 0x4E, 0x50, 0x89 };


        /// <summary>
        /// Gets mime type for specified file
        /// Based on REAL file type checking, not just dummy extensions
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string GetMimeType(byte[] file)
        {
            var mimeDictionary = new Dictionary<string, string>()
            {
                {"Jpg", "image/jpeg"},
                {"Png", "image/png"},

            };

            //Iterate through properties of this object
            foreach (var propertyInfo in typeof(MimeTypeChecker).GetFields(BindingFlags.NonPublic | BindingFlags.Static))
            {
                var value = (byte[])propertyInfo.GetValue(typeof(MimeTypeChecker));
                if (file.Take(value.Length).SequenceEqual(value))
                {
                    return mimeDictionary[propertyInfo.Name];
                }
            }

            //Unknown MIME
            return "application/octet-stream";
        }
    }
}
