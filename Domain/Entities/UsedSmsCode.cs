﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("UsedSmsCodes")]
    public class UsedSmsCode
    {
        public int Id { get; set; }

        public string SmsCode { get; set; }

        public int Value { get; set; }

        [ForeignKey("Advertisement")]
        public int AdvertisementId { get; set; }

        //[ForeignKey("AdvertisementId")]
        public virtual Advertisement Advertisement { get; set; }
    }
}
