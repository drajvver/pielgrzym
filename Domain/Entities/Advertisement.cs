﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Advertisement
    {
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime ExpirationTime { get; set; }
        public string AdContent { get; set; }
        public int BedsAmount { get; set; }
        public double PricePerBed { get; set; }
        public bool IsActive { get; set; }
        public int BedsTaken { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public virtual ICollection<File> ImagesCollection { get; set; }
        
        [Required, ForeignKey("User")]
        public string UserId { get; set; }

        public virtual List<UsedSmsCode> UsedSmsCode { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}