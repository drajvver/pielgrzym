﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class File
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string Directory { get; set; }
        public long Size { get; set; }
        public DateTime UploadTime { get; set; }
        public DateTime ExpireTime { get; set; }
        public virtual Advertisement Advertisement { get; set; }

        [NotMapped]
        public string FullPath
        {
            get { return Directory + @"/" + FileName; }
        }
    }
}