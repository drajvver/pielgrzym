﻿using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using Domain.Helpers;
using Newtonsoft.Json;

namespace Domain.Services
{
    public class CoordinateService
    {
        public CoordinatesHelper.Location GetCoordinates(string fullAddress)
        {
            var jsonResponse =
                new WebClient().DownloadString("http://maps.google.com/maps/api/geocode/json?address=" + fullAddress +
                                               "&sensor=false");
            var coordinatesHelper = JsonConvert.DeserializeObject<CoordinatesHelper>(jsonResponse);

            return coordinatesHelper.Results.Select(p => p.Geometry.Location).FirstOrDefault();
        }


    }
}