﻿
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Domain.Entities;
using Domain.Helpers;
using File = System.IO.File;


namespace Domain.Services
{
    public class UploadFileService
    {
        public static readonly string[] AllowedImageMimeTypes =
        {
            "image/jpeg",
            "image/png",
        };

        public bool IsFileAllowedForUpload(HttpPostedFileBase file, string[] allowedMimeTypes)
        {
            var buf = new byte[30];
            file.InputStream.Read(buf, 0, 30);

            var mime = MimeTypeChecker.GetMimeType(buf);
            return allowedMimeTypes.Contains(mime);
        }

        public Entities.File UploadFile(HttpPostedFileBase file, string directory, Advertisement advertisement)
        {
            var destinationDirectory = new DirectoryInfo(directory);

            var pathString = Path.Combine(destinationDirectory.ToString());

            var fileName = Path.GetFileName(file.FileName);

            //Create directory if it exists
            if (!Directory.Exists(pathString))
            {
                Directory.CreateDirectory(pathString);
            }

            //Preparation for unique name (split filename in extension dot place)
            var lastDotIndex = file.FileName.LastIndexOf('.');
            var fileNameWithoutExtension = file.FileName;
            string fileExtension = null;
            if (lastDotIndex > -1) //If there is any extension
            {
                fileNameWithoutExtension = file.FileName.Substring(0, lastDotIndex);
                fileExtension = file.FileName.Substring(lastDotIndex);
            }

            //Find unique name
            var i = 1;
            while (File.Exists(String.Format("{0}\\{1}", pathString, fileName)))
            {
                var newFileName = fileNameWithoutExtension + '-' + i + fileExtension;
                fileName = newFileName;

                ++i;
            }

            var path = string.Format("{0}\\{1}", pathString, fileName);
            file.SaveAs(path);

            return new Entities.File
            {
                FileName = fileName,
                Directory = "Files",
                Size = file.ContentLength,
                Advertisement = advertisement,
                UploadTime = DateTime.UtcNow,
                ExpireTime = DateTime.UtcNow.AddMonths(12) // TODO: when expire?
            };
        }
    }
}
